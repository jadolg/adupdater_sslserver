from http.server import HTTPServer, SimpleHTTPRequestHandler
import ssl

print('this server an the google files will help you update the SDK of your offline friends')
print('cert password is "qweqwe123!"')
old_hosts = open('/etc/hosts', 'r').read()
open('/etc/hosts','w').write(old_hosts+'\n127.0.0.1       dl.google.com')
# print(old_hosts)
try:
    httpd = HTTPServer(('localhost', 443), SimpleHTTPRequestHandler)


    httpd.socket = ssl.wrap_socket (httpd.socket, 
            keyfile="key.pem", 
            certfile='cert.pem', server_side=True)

    httpd.serve_forever()
except KeyboardInterrupt as ex:
    print('clone server is no longer running...')
finally:
    open('/etc/hosts','w').write(old_hosts)