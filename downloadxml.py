import requests
from os import makedirs
from os.path import dirname, abspath

PREFIX = 'https://dl.google.com/android/repository/'
PATH = 'android/repository/'

proxies = {
    'http': 'socks5h://localhost:1080',
    'https': 'socks5h://localhost:1080'
}

FILES = [
    'repository2-1.xml',
    'addons_list-1.xml',
    'addons_list-2.xml',
    'addons_list-3.xml',
    'sys-img/android/sys-img2-1.xml',
    'sys-img/android-wear/sys-img2-1.xml',
    'sys-img/android-wear-cn/sys-img2-1.xml',
    'sys-img/android-tv/sys-img2-1.xml',
    'sys-img/google_apis/sys-img2-1.xml',
    'sys-img/google_apis_playstore/sys-img2-1.xml',
    'addon2-1.xml',
    'glass/addon2-1.xml',
    'extras/intel/addon2-1.xml'
]

for afile in FILES:
    print('downloading '+afile)
    try:
        r = requests.get(PREFIX+afile, stream=True, proxies=proxies, verify=False)
    except:
        pass
    
    while not r or r.status_code != 200:
        print('retry...')
        try:
            r = requests.get(PREFIX+afile, stream=True, proxies=proxies, verify=False)
        except:
            pass
    
    if r.status_code == 200:
        try:
            directory = dirname(abspath(PATH+afile))
            print('creating '+ directory)
            makedirs(directory)
            print('created directory '+directory)
        except OSError:
            print(directory+' was already there')

        print('writing...', end=' ')
        with open(PATH+afile, 'wb') as f:
            for chunk in r:
                f.write(chunk)
        print('done')
